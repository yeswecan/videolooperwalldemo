#pragma once

#include "ofMain.h"
#include "VideoLooper.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    void exit() {
        delete videoGrabber;
        deleteMovie();
    }
    
    VideoLooper looper;
    
    void deleteMovie() {
        for (int i = 0; i < frames.size(); i++) {
            delete frames[i];
        }
        while (frames.size() > 0) {
            frames.erase(frames.begin());
        }
    }
    
	
    // playing:
    struct frame {
        ofTexture texture;
        unsigned long timestamp;
    };
    vector<frame*> frames;
    float playhead;
    unsigned long movieLength;

    // recording:
    unsigned long lastFrameTimestamp;
    unsigned long currentOffset;
    unsigned long recordStarted;
    
    
    ofPixels pixels;
    
    ofVideoGrabber *videoGrabber;
    bool recording;
    
    
    bool recorded;
};
