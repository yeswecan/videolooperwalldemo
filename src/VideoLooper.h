//
//  VideoLooper.cpp
//  videograbLooper
//
//  Created by zebra on 24/05/15.
//
//

#include <stdio.h>
#include "ofMain.h"

class VideoLooper {
public:
    VideoLooper() {};
    ~VideoLooper() {
        deleteMovie();
    }
    
    ////////
    
    void deleteMovie() {
        for (int i = 0; i < frames.size(); i++) {
            delete frames[i];
        }
        while (frames.size() > 0) {
            frames.erase(frames.begin());
        }
    }
    
    void startRecording() {
        drawRecording = false;

        lastFrameTimestamp = ofGetElapsedTimeMillis();
        recordStarted = ofGetElapsedTimeMillis();
        recording = true;
        
        deleteMovie();
        movieLength = frames.size();
    }
    
    void stopRecording() {
        recording = false;
        movieLength = frames[frames.size() - 1]->timestamp;
    }
    
    void addFrame() {
        ofLog() << "attempt...";
//        float currentMillis = ofGetElapsedTimeMillis() - lastFrameTimestamp;
        frame *newFrame = new frame();
        newFrame->texture.loadData(source, width, height, GL_RGB);
        newFrame->timestamp = ofGetElapsedTimeMillis() - recordStarted;
        
        frames.push_back(newFrame);
//        lastFrameTimestamp = ofGetElapsedTimeMillis();
    }
    
    void setup(ofTexture *textureRef, unsigned char* pixelSource, int width_, int height_) {
        source = pixelSource;
        texture = textureRef;
        
        drawRecording = false;
        
        width = width_; height = height_;
    }
    
    int getFramesCount() {
        return frames.size();
    }
    
    void update() {
        
    }
    
    
    
    void draw(int x, int y, int w, int h) {
        if (!drawRecording) {
            texture->draw(x,y,w,h);
        } else {
            float pIS = (ofGetElapsedTimeMillis() + (int)playheadOffset) % movieLength;
            float playhead_ = ((float)pIS / (float)movieLength);
            int cF = (int)((float)frames.size() * playhead_);
            frames[cF]->texture.draw(x,y,w,h);
        }
    }
    
    void draw(int x, int y) {
        draw(x, y, width, height);
    }
    
    
    bool recording;
    float playheadOffset;
    bool drawRecording;
    
private:
    // playing:
    struct frame {
        ofTexture texture;
        unsigned long timestamp;
    };
    vector<frame*> frames;
    float playhead;
    unsigned long movieLength;
    
    // recording:
    unsigned long lastFrameTimestamp;
    unsigned long currentOffset;
    unsigned long recordStarted;

    
    ////////
    
    typedef unsigned char* RawPixels;
    RawPixels source;
    ofTexture *texture;
    int width, height;
    
    
    
};