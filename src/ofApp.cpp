#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofTexture tex;
    
    videoGrabber = new ofVideoGrabber();
    videoGrabber->listDevices();
    videoGrabber->setDesiredFrameRate(15);
    videoGrabber->initGrabber(640, 480);
    recording = false;
    
//    movieLength = 0;
    
    looper.setup(&(videoGrabber->getTextureReference()), videoGrabber->getPixels(), videoGrabber->getWidth(), videoGrabber->getHeight());
}

//--------------------------------------------------------------
void ofApp::update(){
    videoGrabber->update();
    
    looper.update();
    if (videoGrabber->isFrameNew()) {
        
        if (looper.recording) {
            looper.addFrame();
        }
        lastFrameTimestamp = ofGetElapsedTimeMillis();
    }
    
    if (movieLength != 0) {
        float playheadInSeconds = ofGetElapsedTimeMillis() % movieLength;
        playhead = ((float)playheadInSeconds / (float)movieLength);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(255);
    
    if (!looper.drawRecording) {
        looper.draw(0, 0);
        
        ofDrawBitmapString((looper.recording ? "Recording. Press space to stop" : "Press space to record"), ofPoint(10, ofGetHeight() - 20));
    } else {
        int w = 50; int h = 35;
        for (int i  = 0; i < w; i++) {
            for (int j = 0; j < h; j ++) {
                looper.playheadOffset = (i * 300) + (j * 300);
                looper.draw(i * ((float)ofGetWidth() / (float)w),
                            j * ((float)ofGetHeight() / (float)h),
                            ((float)ofGetWidth() / (float)w) + 1,
                            ((float)ofGetHeight() / (float)h) + 1);
            }
        }
    }
    ofDrawBitmapString(ofToString(looper.getFramesCount()) + " | " + ofToString(playhead), 75, 75);
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if ((key == 32) && (!looper.recording)) {
        ofLog() << "started record";
        looper.startRecording();
    } else if (key == 32){
        ofLog() << "stopped record";
        looper.stopRecording();
        looper.drawRecording = true;
    }
    if (key == 'f') ofToggleFullscreen();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
